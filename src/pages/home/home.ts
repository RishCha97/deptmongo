import { Component } from '@angular/core';
import { NavController , NavParams} from 'ionic-angular';
import { DataService } from '../../providers/data-service';
import { SettingsPage } from '../settings/settings';
import { EmpService } from '../../providers/emp-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    data: any;
    search : any;
    userData : any;
    constructor(public ES : EmpService, public DS: DataService, public navCtrl: NavController, public navPar: NavParams) {
        this.userData =this.navPar.get('userData');
      //  console.log(this.userData[0].Depts);
        console.log(this.userData);
    }
    ionViewWillEnter() {
        console.log('----------Entered in ionViewWillEnter in home.ts---------')
        this.GetAllDept();
        this.updateUserData();
        console.log('----------Exited ionViewWillEnter in home.ts---------')
    }

    updateUserData(){
        console.log('----------Entered in updateUserData in home.ts---------')
        this.ES.loginEmp(this.userData[0].Name).then((data)=>{
            this.userData = data
        })
        console.log('----------Exited updateUserData in home.ts---------')

    }

    GetAllDept() {
        console.log('----------Entered in GetAllDept in home.ts---------')
        this.DS.getAllDept().then((data) => {
            this.data = data;
            console.log(this.data);
        })
        console.log('----------Exited GetAllDept in home.ts---------')
    }

    deleteDept(dept_id , deptName) {
        console.log('----------Entered in deleteDept in home.ts---------')
        this.DS.deleteDept(dept_id).then(() => {
            this.GetAllDept();
        });
        this.ES.deleteDept(this.userData[0]._id , deptName).then(()=>{
            this.updateUserData();
        })
        console.log('----------Exited deleteDept in home.ts---------')
    }

    getItems(ev: any) {
        console.log('----------Entered in getItems in home.ts---------')
        let value = ev.target.value;

        if (value && value.trim() != this.search) {
            this.search = value;
            this.data = this.data.filter((item) => {
                console.log(this.data)
                for (let i in item.employ) {
                    if (item.Name.toLowerCase().indexOf(value.toLowerCase()) > -1 || item.employ[i].Name.toLowerCase().indexOf(value.toLowerCase()) > -1) {
                        return (item.Name.toLowerCase().indexOf(value.toLowerCase()) > -1 || item.employ[i].Name.toLowerCase().indexOf(value.toLowerCase()) > -1);
                    }

                }
            })
        } else {
            this.GetAllDept();
        }
        console.log('----------Exited getItems in home.ts---------')
    }

    delete(dept_id ,deptName, empName){
        console.log('----------Entered in delete in home.ts---------')
        this.DS.deleteEmp(dept_id , empName).then(()=>{
            console.log("refreshing list of employees")
            this.GetAllDept();
            
        })
        this.ES.deleteDept(this.userData[0]._id, deptName).then(()=>{
            this.updateUserData();
        })
        console.log('----------Exited delete in home.ts---------')        
    }

    changePage(){
        console.log('----------Entered in changePage in home.ts---------')
        this.navCtrl.push(SettingsPage , {userData : this.userData});
        console.log('----------Exited changePage in home.ts---------')
    }
}