import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SettingsPage } from '../settings/settings'; 
import { HomePage }  from '../home/home';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1root : HomePage;
  tab2root : SettingsPage;
  userData : any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.navCtrl.setRoot(HomePage);
    this.userData = this.navParams.get('userData');
  }

}
