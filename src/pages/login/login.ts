import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EmpService } from '../../providers/emp-service';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  loginName : any;
  userData : any;
  data : any;
  constructor(public es : EmpService,public navCtrl: NavController, public navParams: NavParams) {}



  submit(){
    console.log('----------Entered submit in login.ts---------')
    this.es.loginEmp(this.loginName).then((data)=>{
      this.data = data;
      if(this.data.length != 0){
        console.log(data);
        this.userData = data;
        console.log(this.userData);
        this.navCtrl.setRoot(HomePage , { userData : this.userData});
      }
    })
    console.log('----------Exited submit in settings.ts---------')
  }

}
