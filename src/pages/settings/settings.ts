import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DataService } from '../../providers/data-service';
import { EmpService  } from '../../providers/emp-service';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  dept: any;
  data: any;
  userData : any;
  constructor(public ES : EmpService,public DS: DataService, public navCtrl: NavController, public navParams: NavParams) { 
    this.userData = this.navParams.get('userData');
  }

  ionViewWillEnter() {
    console.log('----------Entered ionViewWillEnter in settings.ts---------')
    this.getAllDept();
    console.log('----------Exited ionViewWillEnter in settings.ts---------')
  }

  getAllEmp(){
    console.log('----------Entered getAllEmp in settings.ts---------')
    this.ES.getAllEmp().then((data)=>{
      console.log(data)
    })
    console.log('----------Exited getAllEmp in settings.ts---------')
  }


  getAllDept() {
    console.log('----------Entered getAllDept in settings.ts---------')
    this.DS.getAllDept().then((data) => {
      this.data = data;
    });
    console.log('----------Exited getAllDept in settings.ts---------')
  }

  postData() {
    console.log('----------Entered postData in settings.ts---------')
    let data ={
      "employ" : this.userData[0].Name,
      "dept" : this.dept
    }

    this.ES.addDept(this.userData[0]._id , this.dept ).then(()=>{
      this.getAllEmp();
    });
    this.DS.postData(data).then(() => {
      this.getAllDept();
    });
    console.log('----------Exited postData in settings.ts---------')
  }

  deleteDept(dept_id , deptName) {
    console.log('----------Entered deleteDept in settings.ts---------')
    this.DS.deleteDept(dept_id).then(() => {
      this.getAllDept();
    })

    this.ES.deleteDept(this.userData[0]._id , deptName).then(()=>{
      console.log('ES called')
      this.getAllEmp();
    })
    console.log('----------Exited deleteDept in settings.ts---------')
  }
}
