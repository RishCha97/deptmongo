import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class DataService {
    data: any;
    updateData : any;
    constructor(public http: Http) {
        console.log('----------Entered constructor in data-service---------')
        this.getAllDept();
        console.log('----------Exited constructor in data-service---------')
    }

    getAllDept() {
        console.log('----------Entered getAllDept in data-service---------')
        return new Promise((resolve) => {
            this.http.get('http://localhost:8181/api/Dept').map(res => res.json()).subscribe((data) => {
                this.data = data;
                console.log('----------Exited getAllDept in data-service---------')
                resolve(this.data);
            });
        });
    }

    deleteDept(dept_id) {
        console.log('----------Entered deleteDept in data-service---------')
        let url = 'http://localhost:8181/api/Depts/' + dept_id;

        return new Promise((resolve) => {
            let headers = {
                "content-type": 'application/json'
            }
            this.http.delete(url, headers).subscribe((res) => {
                console.log(res.json());
            });
            console.log('----------Exited deleteDept in data-service---------')
            resolve(this.data);
        })

    }

    deleteEmp(dept_id, empName) {
        console.log('----------Entered constructor in data-service---------')
        let url = 'http://localhost:8181/api/dept/' ;

        return new Promise((resolve) => {

            let headers = {
                "content-type": 'application/json'
            };

            let index;

            for(let i in this.data){
                if(this.data[i]._id == dept_id){
                    for(let b in this.data[i].employ){
                        if(this.data[i].employ[b].Name == empName){
                            index = b;
                            this.data[i].employ.splice(index,1);
                            console.log(this.data[i]);
                            this.updateData = this.data[i];
                        }
                    }
                }
            }


            this.http.post(url, this.updateData).subscribe((res) => {
                console.log(res.json());
            });
            console.log('----------Exited deleteEmp in data-service---------')
            resolve(this.data);
        })

    }

    postData(data){
        console.log('----------Entered constructor in data-service---------')
     let url = 'http://localhost:8181/api/dept/';

        return new Promise((resolve) => {
            let headers = {
                "content-type": 'application/json'
            }
            let flag = 0;
            for(let i in this.data){
                console.log(this.data[i]);
                if(this.data[i].Name == data.dept){
                    this.data[i].employ.push(data.employ);
                    this.updateData = this.data[i];
                    console.log(this.updateData);
                    flag = 1;
                }
            }
            if(flag == 0){
                let updatedata = {
                    "Name" : data.dept ,
                    "employ": [{Name : data.employ}]
                }
                this.updateData = updatedata;
                console.log(this.updateData);
                console.log(updatedata);
            }

            this.http.post(url, this.updateData).subscribe((res) => {
                console.log(res.json());
            });
            console.log('----------Exited postData in data-service---------')
            resolve(this.data);
        })

    }

}
