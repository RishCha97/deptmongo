import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class EmpService {
  data : any;
  user : any;
  updateData : any;
  constructor(public http: Http) {
    console.log('----------Entered constructor in emp-service---------')
    this.getAllEmp();
    console.log('----------Exiting constructor in emp-service---------')
  }

      getAllEmp() {
          console.log('----------Entered getAllEmp in emp-service---------')
        return new Promise((resolve) => {
            this.http.get('http://localhost:8080/api/emp').map(res => res.json()).subscribe((data) => {
                this.data = data;
                console.log('----------Exiting getAllEmp in emp-service---------')
                resolve(this.data);
            });
        });
        
    }

      loginEmp(name){
          console.log('----------Entered loginEmp in emp-service---------')
        let url = 'http://localhost:8080/api/emp/' + name;
         return new Promise((resolve) => {
            this.http.get(url).map(res => res.json()).subscribe((data) => {
                this.user = data;
                console.log(data);
                console.log(this.user);
                try{
                
                this.user._id 
                    
                }
                catch(err){
                    console.log(err);
                    this.addEmp(name);
                }
               
                console.log('----------Exiting loginEmp in emp-service---------')
                resolve(this.user);
            })
        }) 
      }

      addEmp(name){
          let data = {
              "Name": name,
              "Depts" : []
          }
          return new Promise((resolve)=>{
              this.http.post('http://localhost:8080/api/emp', data).map(res => res.json()).subscribe((data)=>{
                  this.user = data;
              })
              resolve(this.user);
          })
      }

      postData(postData){
          console.log('----------Entered postData in emp-service---------')
          let url = 'http://localhost:8080/api/emp/';
          for(let i in this.data){
              if(this.data[i].Name == postData.employ){
                  this.data[i].Depts.push(postData.dept);
                  this.updateData = this.data[i];
              }
          }
           return new Promise((resolve) => {
            this.http.post(url , this.updateData).map(res => res.json()).subscribe((data) => {
                this.user = data;
                console.log('----------Exiting postData in emp-service---------')
                resolve(this.user);
            });
        }).then(()=>{
            this.getAllEmp();
        })
      }

      /**
       * delete a single department from employee data
       */

    deleteDept(emp_id, deptName) {
        console.log('----------Entered deleteDept in emp-service---------')

        let url = 'http://localhost:8080/api/emp/' ;

        return new Promise((resolve) => {

            let headers = {
                "content-type": 'application/json'
            };

            let index;

            for(let i in this.data){
                if(this.data[i]._id == emp_id){
                    for(let b in this.data[i].Depts){
                        if(this.data[i].Depts[b] == deptName){
                            index = b;
                            this.data[i].Depts.splice(index,1);
                            console.log(this.data[i]);
                            this.updateData = this.data[i];
                        }
                    }
                }
            }


            this.http.post(url, this.updateData).subscribe((res) => {
                console.log(res.json());
                this.getAllEmp();
            });
            console.log('----------Exiting deleteDept in emp-service---------')
            resolve(this.data);
        }).then(()=>{
        this.getAllEmp();
        })

    }

    addDept(emp_id , deptName){
        console.log('----------Entered addDept in emp-service---------')

         let url = 'http://localhost:8080/api/emp/' ;

        return new Promise((resolve) => {

            let headers = {
                "content-type": 'application/json'
            };

            let index;

            for(let i in this.data){
                console.log(this.data[i]);
                if(this.data[i]._id == emp_id){
                    console.log(this.data[i]);
                    this.data[i].Depts.push(deptName);
                    this.updateData = {
                        "Name" : this.data[i].Name,
                        "Depts" : this.data[i].Depts
                    };
                }
            }


            this.http.post(url, this.updateData).subscribe((res) => {
                console.log(res.json());
                this.getAllEmp();
            });
            console.log('----------Exiting addDept in emp-service---------')
            resolve(this.data);
        }).then(()=>{
            this.getAllEmp();
        })
    }


}
