// Set up
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');

// Configuration
mongoose.connect('mongodb://localhost/EmpDept');

app.use(bodyParser.urlencoded({
  extended: false
})); // Parses urlencoded bodies
app.use(bodyParser.json()); // Send JSON responses
app.use(logger('dev')); // Log requests to API using morgan
app.use(cors());

// Models
var Emp = mongoose.model('Emp', {
  Name: String,
  Depts: [String]
});


app.get('/api/emp', function (req, res) {

  Emp.find(function (err, dept) {

    if (err)
      res.send(err)

    res.json(dept);
  });
});

app.get('/api/emp/:name', function (req, res) {

  Emp.find({
    Name : req.params.name
  },function (err, dept) {

    if (err)
      res.send(err)

    res.json(dept);
  });
});

app.post('/api/emp', function (req, res) {
console.log(req.body);
  Emp.update({
    Name: req.body.Name
  },{
    Depts: req.body.Depts
  },{
    upsert : true
  }, function (err, depts) {
    if (err)
      res.send(err);

    Emp.find(function (err, depts) {
      if (err)
        res.send(err)
      res.json(depts);
    });
  });

});

app.delete('/api/emp/:emp_id', function (req, res) {
  Emp.remove({
    _id: req.params.emp_id
  }, function (err, depts) {
    if (err)
      res.send(err);

    Emp.find(function (err, depts) {
      if (err)
        res.send(err)
      res.json(depts);
    });
  });
});



app.delete('/api/depts/:emp_id:/:dept_id', function (req, res) {
  //let index = req.body.employ.indexOf({_id : })
  console.log(req.body)
  Emp.update({
    Name: req.body.Name
  }, {
    Depts: req.body.Depts
  }, {
    upsert: true
  }, function (err, depts) {
    if (err) {
      res.send(err);
    }
    Emp.find(function (err, depts) {
      if (err)
        res.send(err)
      res.json(depts);
    });
  });
});


app.listen(8080);
console.log("App listening on port 8080");

