// Set up
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');

// Configuration
mongoose.connect('mongodb://localhost/Dept');

app.use(bodyParser.urlencoded({
  extended: false
})); // Parses urlencoded bodies
app.use(bodyParser.json()); // Send JSON responses
app.use(logger('dev')); // Log requests to API using morgan
app.use(cors());

// Models
var Dept = mongoose.model('Dept', {
  Name: String,
  employ: [{
    _id: Number,
    Name: String
  }]
});


app.get('/api/dept', function (req, res) {

  Dept.find(function (err, dept) {

    if (err)
      res.send(err)

    res.json(dept);
  });
});

app.post('/api/depts', function (req, res) {

  Dept.create({
    Name: req.body.Name,
    type: req.body.type,
    empnum: req.body.empnum,
    employ: req.body.employ,
    // done : false
  }, function (err, depts) {
    if (err)
      res.send(err);

    Dept.find(function (err, depts) {
      if (err)
        res.send(err)
      res.json(depts);
    });
  });

});

app.delete('/api/depts/:depts_id', function (req, res) {
  Dept.remove({
    _id: req.params.depts_id
  }, function (err, depts) {
    if (err)
      res.send(err);

    Dept.find(function (err, depts) {
      if (err)
        res.send(err)
      res.json(depts);
    });
  });
});

app.post('/api/dept', function (req, res) {
  console.log(req.body);
  Dept.update({
      Name: req.body.Name
    }, {
      employ: req.body.employ
    }, {
      upsert: true
    }, function (err, depts) {
      Dept.find(function (err, depts) {
        if (err)
          res.send(err)
        res.json(depts);
      });
    }

  )
})

app.delete('/api/depts/:depts_id:/:emp_id', function (req, res) {
  //let index = req.body.employ.indexOf({_id : })
  console.log(req.body)
  Dept.update({
    Name: req.body.Name,
    type: req.body.type
  }, {
    employ: req.body.employ
  }, {
    upsert: true
  }, function (err, depts) {
    if (err) {
      res.send(err);
    }
    Dept.find(function (err, depts) {
      if (err)
        res.send(err)
      res.json(depts);
    });
  });
});


app.listen(8181);
console.log("App listening on port 8181");
